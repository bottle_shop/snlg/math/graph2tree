PyTorch implementation of Graph based Math Word Problem solver described in our ACL 2020 paper Graph-to-Tree Learning for Solving Math Word Problems. In this work, we propose a solution for Math Word Problem Solving via graph neural network.

## Steps to run the experiments

### Requirements
* ``Python 3.6 ``
* ``>= PyTorch 1.0.0``

For more details, please refer to requiremnt file.

### Training
* python run_seq2tree_graph.py


### Acknowledgement
```
@inproceedings{zhang2020graph,
  title={Graph-to-Tree Learning for Solving Math Word Problems},
  author={Zhang, Jipeng and Wang, Lei and Lee, Roy Ka-Wei and Bin, Yi and Wang, Yan and Shao, Jie
  and Lim, Ee-Peng},
  booktitle={Proceedings of the 58th Annual Meeting of the Association for Computational Linguistics},
  pages={3928--3937},
  year={2020}
}
```